var express = require('express')
var path = require('path');
var app = express();

app.use(express.static(path.resolve(__dirname + '/../app')));
app.route('/*')
	.get(function(req, res) {
		res.sendFile(path.resolve(__dirname + '/../app/index.html'));
	});

app.listen(3000, function () {
	console.log('Express server listening on 3000');
});