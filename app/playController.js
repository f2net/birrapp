(function () {
	

	var app = angular.module('birrApp');

	var playController = function($scope) {

		$scope.brewery = {
			name: 'Birrificio Elav',
			address: 'Via Pinco Pallino, 7',
			city: 'Comun Nuovo',
			province: 'BG',
			imageUrl: 'http://www.f2.net/birrapp/logo_elav.png',
			beers: [
				{name: 'Grunge', style: 'American IPA', likes: 3},
				{name: 'Progressive', style: 'Barley Wine', likes: 5},
				{name: 'Dark Metal', style: 'Imperial Stout', likes: 0}
			]
		};


		$scope.addLike = function (beer) {
			beer.likes = beer.likes + 1;
		};

		$scope.addBeer = function(newBeer) {
			newBeer.likes = 0;
			$scope.brewery.beers.push(newBeer);
			$scope.newBeer = {};
		};

	};


	app.controller('playController', playController);



})();