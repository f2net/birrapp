(function () {
	
	var app = angular.module('birrApp');

	var beerDetailsController = function($scope, $routeParams, apiService) {

		var beerId = $routeParams.beerId;

		var onResult = function (beer) {
			$scope.beer = beer;
		};
		
		var onError = function (error) {
			console.log(error);
		};
		
		var promise = apiService.getBeer(beerId)
			.then(onResult, onError);

	};

	app.controller('beerDetailsController', beerDetailsController);

})();