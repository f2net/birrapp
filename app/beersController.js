(function () {
	

	var app = angular.module('birrApp');

	var beersController = function($scope, apiService) {

		var onResult = function (beers) {
			$scope.beers = beers;
		};
		
		var onError = function (error) {
			console.log(error);
		};
		
		var promise = apiService.getBeers()
			.then(onResult, onError);


		$scope.orders = [
			{field:'name', description:'Nome birra'},
			{field:'breweryName', description:'Birrificio'},
			{field:'abv', description:'Gradi alcolici'}
		];

		$scope.order = 'name';

	};


	app.controller('beersController', beersController);



})();