(function () {

	var app = angular.module('birrApp');

	var apiService = function ($http) {

		var onResult = function (response) {
			return response.data;
		};

		var getBeers = function () {
			return $http.get('http://api.birrapp.it/api/beers')
				.then(onResult);
		};

		var getBeer = function (beerId) {
			return $http.get('http://api.birrapp.it/api/beers/' + beerId)
				.then(onResult);
		};

		var getBreweries = function () {
			return $http.get('http://api.birrapp.it/api/breweries')
				.then(onResult);
		};

		var getBrewery = function (breweryId) {
			return $http.get('http://api.birrapp.it/api/breweries/' + breweryId)
				.then(onResult);
		};

		var rateBeer = function (beerId, rating) {
			var payload = {
				rate: rating
			};

			return $http.post('http://api.birrapp.it/api/beers/' + beerId + '/rate', payload)
				.then(onResult);
		};

		return {
			getBeers: getBeers,
			getBeer: getBeer,
			getBreweries: getBreweries,
			getBrewery: getBrewery,
			rateBeer: rateBeer
		};
	};


	app.factory('apiService', apiService);


})();