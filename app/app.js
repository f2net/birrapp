(function () {

	var app = angular.module('birrApp', ['ngRoute']);

	app.config(function ($routeProvider) {

		$routeProvider
			.when('/beers', {
				templateUrl: 'beers.html',
				controller: 'beersController'
			})
			.when('/beer/:beerId', {
				templateUrl: 'beerDetails.html',
				controller: 'beerDetailsController'
			})
			.otherwise({
				redirectTo: '/beers'
			});
	});

})();